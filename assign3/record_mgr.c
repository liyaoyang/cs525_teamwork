#include "record_mgr.h"

#include <string.h>

#include "table_file.h"
#include "storage_mgr.h"
#include "buffer_mgr.h"
#include "expr.h"
#include "util.h"

// manage openeds tables
TableFile** g_tables;
int g_tables_len;

///////////////////////////////////////////////////////////////////////////////

RC initRecordManager(void *mgmtData) {
    // storage manager
    initStorageManager();

    // init opened tables array
    g_tables = malloc(1024 * sizeof(TableFile*));
    g_tables_len = 0;

    return RC_OK;
}

RC shutdownRecordManager() {
    RC rc;

    // close all tables
    int i;
    for (i = 0; i < g_tables_len; i ++) {
        rc = closeTableFile(g_tables[i]);
        CHECK_RC(rc, "shutdownRecordManager: close table");
        g_tables[i] = NULL;
    }
    g_tables_len = 0;

    return RC_OK;
}

///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// table functions

RC createTable (char *name, Schema *schema) {
    ASSERT_NULL(name , "createTable:name");
    ASSERT_NULL(schema , "createTable:schema");

    RC rc = createTableFile(name, schema);
    CHECK_RC(rc, "createTable");

    return RC_OK;
}

RC openTable (RM_TableData *rel, char *name) {
    ASSERT_NULL(rel , "openTable: rel");
    ASSERT_NULL(name , "openTable: name");

    TableFile* f = openTableFile(name);
    rel->name = name;
    rel->schema = f->schema;
    rel->mgmtData = f;
    // g_tables[g_tables_len++] = f;
    return RC_OK;
}

RC closeTable (RM_TableData *rel) {
    ASSERT_NULL(rel , "closeTable: rel");

    TableFile* f = (TableFile*) rel->mgmtData;
    RC rc = closeTableFile(f);
    CHECK_RC(rc, "closeTable");

    rel->name = NULL;
    rel->schema = NULL;
    rel->mgmtData = NULL;
    return RC_OK;
}

RC deleteTable (char *name) {
    ASSERT_NULL(name , "deleteTable");
    RC rc = deleteTableFile(name);
    CHECK_RC(rc, "deleteTable");
    return RC_OK;
}

int getNumTuples (RM_TableData *rel) {
    ASSERT_NULL(rel , "getNumTuples");
    TableFile* f = (TableFile*) rel->mgmtData;
    return f->totalNumTuples;
}

///////////////////////////////////////////////////////////////////////////////
// handling records in a table

RC insertRecord (RM_TableData *rel, Record *record) {
    ASSERT_NULL(rel , "insertRecord:rel");
    ASSERT_NULL(record , "insertRecord:record");
    TableFile* f = (TableFile*) rel->mgmtData;
    return tableFileInsertRecord(f, record);
}

RC deleteRecord (RM_TableData *rel, RID id) {
    ASSERT_NULL(rel , "insertRecord:rel");
    TableFile* f = (TableFile*) rel->mgmtData;
    return tableFileDeleteRecord(f, id);
}

RC updateRecord (RM_TableData *rel, Record *record) {
    ASSERT_NULL(rel , "insertRecord:rel");
    ASSERT_NULL(record , "insertRecord:record");
    TableFile* f = (TableFile*) rel->mgmtData;
    return tableFileUpdateRecord(f, record);
}

RC getRecord (RM_TableData *rel, RID id, Record *record) {
    ASSERT_NULL(rel , "insertRecord:rel");
    ASSERT_NULL(record , "insertRecord:record");
    TableFile* f = (TableFile*) rel->mgmtData;
    return tableFileGetRecord(f, id, record);
}

///////////////////////////////////////////////////////////////////////////////
// scans

typedef struct ScanIterator {
    Expr* cond;
    RID currID;
    Block* block;
    TableFile* f;
} ScanIterator;

RC startScan (RM_TableData *rel, RM_ScanHandle *scan, Expr *cond) {
    ASSERT_NULL(rel, "startScan:rel");
    ASSERT_NULL(scan, "startScan:scan");
    
    ScanIterator* iter = malloc(sizeof(ScanIterator));
    iter->cond = cond;
    iter->currID.page = 1;
    iter->currID.slot = -1;
    iter->f = (TableFile*) rel->mgmtData;
    iter->block = pinBlock(iter->f, iter->currID.page);
    
    scan->rel = rel;
    scan->mgmtData = iter;
    return RC_OK;
}

RC closeScan (RM_ScanHandle *scan) {
    ASSERT_NULL(scan, "closeScan:scan");

    ScanIterator* iter = (ScanIterator*) scan->mgmtData;
    if (iter->block != NULL) {
        unpinBlock(iter->f, iter->block);
        iter->block = NULL;
    }

    return RC_OK;
}

RC _toNextRecord(ScanIterator* iter) {
    // next record
    while (1) {
        for (int i = iter->currID.slot+1; i < iter->block->capacity; i++) {
            int j = i / 8;
            int k = i % 8;
            int mark = (iter->block->slotMarks[j] >> k) & 1;
            if (mark == 1) {
                // find a record
                iter->currID.slot = i;
                return RC_OK;
            }
        }
        
        // end of file
        if (iter->currID.page + 1 >= getTotalNumPages(iter->f->pool)) {
            iter->block = NULL;
            return RC_RM_NO_MORE_TUPLES;
        }

        iter->currID.page += 1;
        // load next block
        unpinBlock(iter->f, iter->block);
        iter->block = pinBlock(iter->f, iter->currID.page);
    }

    return RC_RM_NO_MORE_TUPLES;
}

RC next (RM_ScanHandle *scan, Record *record) {
    ASSERT_NULL(scan, "next:scan");
    ASSERT_NULL(record, "next:record");

    RC rc;
    ScanIterator* iter = (ScanIterator*) scan->mgmtData;
    
    if (iter->block == NULL) {  // End of file
        return RC_RM_NO_MORE_TUPLES;
    }
    
    Value* result;
    while (1) {
        rc = _toNextRecord(iter);
    
        if (rc != RC_OK) {  // no more record
            return rc;
        }
        rc = getRecord(scan->rel, iter->currID, record);
        CHECK_RC(rc, "next:getRecord");
        
        // check condition
        result = NULL;
        rc = evalExpr(record, iter->f->schema, iter->cond, &result);
        CHECK_RC(rc, "next:evalExpr");

        ASSERT_NULL(result, "next:result");
        if (result->dt != DT_BOOL) {
            printf("result->dt != DT_BOOL\n");
            exit(1);
        }

        // not satifiy the condition
        if (!result->v.boolV) {
            continue;
        }

        // found record
        break;
    }
    return RC_OK;
}

///////////////////////////////////////////////////////////////////////////////
// Schema functions

int _getAttrSize(Schema *s, int i) {
    ASSERT_NULL(s, "getAttrSize");
    int size = 0;
    switch(s->dataTypes[i]) {
        case DT_INT:
            size = sizeof(int);
            break;
        case DT_STRING:
            size = s->typeLength[i];
            break;
        case DT_FLOAT:
            size = sizeof(float);
            break;
        case DT_BOOL:
            size = sizeof(char);
            break;
    }
    return size;
}

int getRecordSize(Schema *s) {
    ASSERT_NULL(s, "getRecordSize");
    int i;
    int total_size = 0;
    for (i = 0; i < s->numAttr; i++) {
        total_size += _getAttrSize(s, i);
    }
    return total_size;
}

Schema *createSchema(int numAttr, char **attrNames, DataType *dataTypes, int *typeLength, int keySize, int *keys) {
    ASSERT_NULL(attrNames, "createSchema: attrnames");
    ASSERT_NULL(dataTypes, "createSchema: dataTypes");
    ASSERT_NULL(typeLength, "createSchema: typeLength");
    ASSERT_NULL(keys, "createSchema: keys");
    
    int i;
    Schema* s = malloc(sizeof(Schema));

    // malloc arrays for attributes
    s->numAttr = numAttr;
    s->attrNames = malloc(numAttr * sizeof(char*));
    s->dataTypes = malloc(numAttr * sizeof(DataType));
    s->typeLength = malloc(numAttr * sizeof(int));

    // copy data for attributes
    for (i = 0; i < numAttr; i++) {
        s->dataTypes[i] = dataTypes[i];
        
        s->typeLength[i] = typeLength[i];

        s->attrNames[i] = malloc(sizeof(char) * (strlen(attrNames[i]) + 1));
        strcpy(s->attrNames[i], attrNames[i]);
    }
    
    // copy for keys
    s->keySize = keySize;
    s->keyAttrs = malloc(keySize * sizeof(int));
    for (i = 0; i < keySize; i++) {
        s->keyAttrs[i] = keys[i];
    }
    
    return s;
}

RC freeSchema(Schema *s) {
    ASSERT_NULL(s, "freeSchema");
    int i;
    // free names
    for (i = 0; i < s->numAttr; i++) {
        free(s->attrNames[i]);
    }

    // free arrays
    free(s->attrNames);
    free(s->dataTypes);
    free(s->typeLength);

    // free keys
    free(s->keyAttrs);
    
    free(s);
    return RC_OK;
}

///////////////////////////////////////////////////////////////////////////////
// Record functions

RC createRecord(Record **r, Schema *s) {
    ASSERT_NULL(s, "createRecord:s");
    ASSERT_NULL(r, "createRecord:r");

    int size = getRecordSize(s);
    *r = malloc(sizeof(Record));
    (*r)->data = malloc(sizeof(char) * size);
    return RC_OK;
}

RC freeRecord(Record *r) {
    ASSERT_NULL(r, "freeRecord:r");
    free(r->data);
    free(r);
    return RC_OK;
}

int _getAttrOffset(Schema *s, int attrNum) {
    ASSERT_NULL(s, "getAttrOffset:s");
    int offset = 0;
    int i;
    for (i = 0; i < attrNum; i++) {
        offset += _getAttrSize(s, i);
    }
    return offset;
}

RC getAttr(Record *r, Schema *s, int attrNum, Value **v) {
    ASSERT_NULL(s, "getAttr:s");
    ASSERT_NULL(r, "getAttr:r");
    int offset = _getAttrOffset(s, attrNum);
    int size = _getAttrSize(s, attrNum);

    *v = malloc(sizeof(Value));
    (*v)->dt = s->dataTypes[attrNum];
    
    // copy value from r->data[offset ~ offset + size]
    if ((*v)->dt == DT_STRING) {
        (*v)->v.stringV = (char *) malloc(size + 1);
        memcpy((*v)->v.stringV, r->data + offset, size);
        (*v)->v.stringV[size] = '\0';
    } else {
        memcpy(&((*v)->v), r->data + offset, size);
    }

    // char* rs = serializeRecord(r, s);
    // printf("getAttr : %s, offset %d, size %d\n", rs, offset, size);
    return RC_OK;
}

RC setAttr(Record *r, Schema *s, int attrNum, Value *v) {
    ASSERT_NULL(s, "setAttr:s");
    ASSERT_NULL(r, "setAttr:r");
    int offset = _getAttrOffset(s, attrNum);
    int size = _getAttrSize(s, attrNum);
    // copy value to r->data[offset ~ offset + size]
    if (v->dt == DT_STRING) {
        memcpy(r->data + offset, v->v.stringV, size);
    } else {
        memcpy(r->data + offset, &(v->v), size);
    }

    // char* rs = serializeRecord(r, s);
    // printf("setAttr : %s, offset %d, size %d\n", rs, offset, size);
    return RC_OK;
}
