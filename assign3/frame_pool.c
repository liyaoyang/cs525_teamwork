#include "frame_pool.h"

#include <stdlib.h>
#include "util.h"
#include "replacement_strategy.h"

BM_FramePool* newFramePool(int maxFrames, ReplacementStrategy strategy) 
{
    BM_FramePool* pool = calloc(1, sizeof(BM_FramePool));

    pool->maxFrames = maxFrames;
    pool->usedFrames = 0;
    pool->strategy = strategy;

    pool->frames = calloc(maxFrames, sizeof(BM_Frame));
    for (int i = 0; i < pool->maxFrames; i++) {
        pool->frames[i].pageNum = NO_PAGE;  // set as not in use
        pthread_mutex_init(&pool->frames[i].lock, NULL);    // init frame lock
    }

    pool->map = newMap(maxFrames);

    pthread_mutex_init(&pool->lock, NULL);

    return pool;
}

void freeFramePool(BM_FramePool* pool) 
{
    ASSERT_NULL(pool, "freeFramePool");

    // destroy locks
    pthread_mutex_destroy(&pool->lock);
    for (int i = 0; i < pool->maxFrames; i++) {
        pthread_mutex_destroy(&pool->frames[i].lock);
    }

    // free memory
    free(pool->frames);
    freeMap(pool->map);
    free(pool);
}

// request a frame by replacement strategy.
BM_Frame* framePoolRequest(BM_FramePool* pool)
{
    ASSERT_NULL(pool, "framePoolRequest");

    pthread_mutex_lock(&pool->lock);

    // look for empty frame
    if (pool->usedFrames < pool->maxFrames) {
        for (int i = 0; i < pool->maxFrames; i++) {
            if (pool->frames[i].pageNum == NO_PAGE) {
                pool->usedFrames++;
                pthread_mutex_unlock(&pool->lock);
                return &pool->frames[i];
            }
        }
        // Never reach here
        printf("WARNING: pool->usedFrames < pool->maxFrames, but didn't find any empty frame.");
        pool->usedFrames = pool->maxFrames;
    }

    // try to apply replacement strategy to get a frame
    BM_Frame* frame =  evict(pool->strategy, pool);

    pthread_mutex_unlock(&pool->lock);
    return frame;
}

// Get a frame from pool by pageNum. Return NULL if frame is not in pool.
BM_Frame* framePoolGet(BM_FramePool* pool, int pageNum) 
{
    ASSERT_NULL(pool, "getFrame");

    pthread_mutex_lock(&pool->lock);

    int index = mapGet(pool->map, pageNum);
    if (index == MAP_NOT_FOUND) {
        pthread_mutex_unlock(&pool->lock);
        return NULL;
    }
    
    BM_Frame* frame = &pool->frames[index];
    
    pthread_mutex_unlock(&pool->lock);

    return frame;
}

// Associate a frame with pageNum.
void framePoolSet(BM_FramePool* pool, BM_Frame* frame, int pageNum) 
{
    ASSERT_NULL(pool, "setFrame:pool");
    ASSERT_NULL(frame, "setFrame:frame");

    pthread_mutex_lock(&pool->lock);

    int index = frame - pool->frames; // calculate the index of frame in array
    
    if (frame->pageNum != NO_PAGE) {
        // reset map of pageNum
        mapPut(pool->map, frame->pageNum, NO_PAGE);
    }
    
    frame->pageNum = pageNum;

    // maping pageNum with index
    mapPut(pool->map, pageNum, index);

    pthread_mutex_unlock(&pool->lock);
}
