Overview:
    We use the first block of a table file to store metadata of the table,
    such as schema, totalNumTuples etc.
    We reserved some header in front of block, such as numTuples of the block,
    free slot management.

Task Assignment:
    Jing Chen   A20381546:  Schema operations and Bytes buffer.
    Yuan Ma     A20394062:  Record scan operations.
    Qi Gao      A20409777:  Record management.
    Liyao Yang  A20411097:  TableFile and free space management.

Makefile:
    Use the following commands
    make        Build sorece files and generate a binary file 'test_assign2'
    make run    Build and run test file.
    make clean  Clean object files.

Code organization:
    table_file.c
        Implementation of table management, including metadata, block header,
        and freespace management.

    bytes_buffer.c
        Utility of bytes read and write operations.

    record_mgr.c
        Using table_file underneath to implement record manager.
