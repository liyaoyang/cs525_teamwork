#ifndef BYTES_BUFFER_H
#define BYTES_BUFFER_H

typedef struct Bytes {
    char* buff;
    int len;
    int idx;
} Bytes;

Bytes* newBytes(char* buff, int len);

int seekBytes(Bytes* s, int offset);

int writeBytes(Bytes* s, char *data, int len);
int writeBytesInt(Bytes* s, int v);
int writeBytesBool(Bytes* s, int b);

int readBytes(Bytes* s, char *data, int len);
int readBytesInt(Bytes* s, int* v);
int readBytesBool(Bytes* s, int* b);

#endif