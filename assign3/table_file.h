#ifndef TABLE_FILE_H
#define TABLE_FILE_H

#include "dberror.h"
#include "tables.h"
#include "buffer_mgr.h"
#include "bytes_buffer.h"

typedef struct Block {
    int pageNum;            // page number
    BM_PageHandle* page;    // page data    
    Bytes* buf;      // string buffer

    int offset;
    int capacity;           // from metadatas
    int recordSize;

    int numTuples;          // num of tuples in block

    int slotSize;           
    unsigned char *slotMarks;   // 0 free, 1 in use.

    // free space queue pointers
    int nextAvaliableBlock;     // next block where has some free spaces
} Block;

typedef struct TableFile {
    BM_BufferPool* pool;
    Schema* schema;

    // int totalNumPages;
    int totalNumTuples;  // TODO

    int headAvaliableBlock;
    int tailAvaliableBlock;

    int recordOffset;       // size of block header
    int recordCapacity;     // max num of records in a block
    int recordSize;
} TableFile;

RC createTableFile(char* fileName, Schema* s);

TableFile* openTableFile(char* fileName);
RC closeTableFile(TableFile* t);

RC deleteTableFile (char *fileName);

Block* pinBlock(TableFile* f, int pageNum);
RC unpinBlock(TableFile* f, Block* b);

RC tableFileInsertRecord(TableFile* t, Record *record);
RC tableFileDeleteRecord(TableFile* t, RID id);
RC tableFileUpdateRecord(TableFile* t, Record *record);
RC tableFileGetRecord(TableFile* t, RID id, Record *record);

#endif