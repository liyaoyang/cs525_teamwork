#include "table_file.h"

#include <string.h>

#include "storage_mgr.h"
#include "buffer_mgr.h"
#include "record_mgr.h"
#include "bytes_buffer.h"
#include "util.h"

#define META_PAGE   0
#define PAGE_BUFFER_SIZE  200
#define PAGE_BUFFER_STRATEGY    RS_LRU
#define MARK_BITS   8

///////////////////////////////////////////////////////////////////////////////

void _printMetadata(TableFile* f) {
    char* s = serializeSchema(f->schema);
    printf("TableFile %d: schema [%s] queue [%d, %d] offset %d, cap %d, record size %d \n", f->totalNumTuples, s, 
        f->headAvaliableBlock, f->tailAvaliableBlock, f->recordOffset, f->recordCapacity, f->recordSize);
    free(s);
}

void _printBlock(Block* b) {
    printf("Block %d (%d): avi %d, slots %d [", b->pageNum, b->numTuples, b->nextAvaliableBlock, b->slotSize);
    for (int i = 0; i < b->slotSize; i++) {
        for (int j = 0; j < MARK_BITS; j++) {
            printf("%d", (b->slotMarks[i] >> j) & 1);
        }
        printf(" ");
    }
    printf("\n");
}

///////////////////////////////////////////////////////////////////////////////

int _writeSchema(Bytes* sb, Schema* s) {
    ASSERT_NULL(sb, "writeSchema:sb");
    ASSERT_NULL(s, "writeSchema:s");
    int i;
    // num of attributes
    writeBytesInt(sb, s->numAttr);
    
    // attributes
    for (i = 0; i < s->numAttr; i++) {
        // name
        int len = strlen(s->attrNames[i]);
        writeBytesInt(sb, len);
        writeBytes(sb, s->attrNames[i], len);
        // data type
        writeBytesInt(sb, (int)s->dataTypes[i]);
        // attr length
        writeBytesInt(sb, s->typeLength[i]);
    }

    // keys
    writeBytesInt(sb, s->keySize);
    for (i = 0; i < s->keySize; i++) {
        writeBytesInt(sb, s->keyAttrs[i]);
    }

    return sb->idx;
}

int _readSchema(Bytes* sb, Schema* s) {
    ASSERT_NULL(s, "readSchema:s");
    ASSERT_NULL(sb, "readSchema:sb");
    // num of attributes
    int numAttr;
    readBytesInt(sb, &numAttr);

    // attributes
    char** names = malloc(numAttr * sizeof(char*));
    DataType* dataTypes = malloc(numAttr * sizeof(DataType));
    int* typeLength = malloc(numAttr * sizeof(int));

    int i;
    for (i = 0; i < numAttr; i++) {
        // name
        int len;
        readBytesInt(sb, &len);
        names[i] = malloc((len+1) * sizeof(char));
        readBytes(sb, names[i], len);
        names[i][len] = '\0';
        // data type
        int type;
        readBytesInt(sb, &type);
        dataTypes[i] = type;
        // attr length
        readBytesInt(sb, &typeLength[i]);
    }

    // keys
    int keySize;
    readBytesInt(sb, &keySize);
    
    int* keyAttrs;
    keyAttrs = malloc(keySize * sizeof(int));
    for (i = 0; i < keySize; i++) {
        readBytesInt(sb, &keyAttrs[i]);
    }

    s->numAttr = numAttr;
    s->attrNames = names;
    s->dataTypes = dataTypes;
    s->typeLength = typeLength;
    s->keyAttrs = keyAttrs;
    s->keySize = keySize;

    return sb->idx;
}

///////////////////////////////////////////////////////////////////////////////

RC loadMetadata(TableFile* f) {
    ASSERT_NULL(f, "loadMetadata:f");

    BM_PageHandle* page = MAKE_PAGE_HANDLE();

    RC rc = pinPage(f->pool, page, META_PAGE);
    CHECK_RC(rc, "load metadata: pinPage");

    Bytes* sb = newBytes(page->data, PAGE_SIZE);

    // read metadata from page
    f->schema = malloc(sizeof(Schema));
    _readSchema(sb, f->schema);

    // read attributes
    // f->totalNumPages = getTotalNumPages(f->pool);
    readBytesInt(sb, &f->totalNumTuples);

    // queue of avaliable block
    readBytesInt(sb, &f->headAvaliableBlock);
    readBytesInt(sb, &f->tailAvaliableBlock);

    // init other attributes
    f->recordSize = getRecordSize(f->schema);
    f->recordOffset = 512;    // reserve some space for header
    f->recordCapacity = (PAGE_SIZE - f->recordOffset) / f->recordSize; 

    unpinPage(f->pool, page);
    free(page);
    free(sb);
    return RC_OK;
}

RC initMetadata(BM_BufferPool* pool, Schema* s) {
    BM_PageHandle* page = MAKE_PAGE_HANDLE();

    RC rc = pinPage(pool, page, META_PAGE);
    CHECK_RC(rc, "update metadata: pinPage");

    Bytes* sb = newBytes(page->data, PAGE_SIZE);

    // write metadata to page
    _writeSchema(sb, s);

    writeBytesInt(sb, 0);   // totalNumTuples
    writeBytesInt(sb, -1);  // headAvaliableBlock
    writeBytesInt(sb, -1);  // tailAvaliableBlock

    markDirty(pool, page);
    unpinPage(pool, page);
    free(page);
    free(sb);
    return RC_OK;
}

RC updateMetadata(TableFile* f) {
    ASSERT_NULL(f, "updateMetadata:f");

    BM_PageHandle* page = MAKE_PAGE_HANDLE();

    RC rc = pinPage(f->pool, page, META_PAGE);
    CHECK_RC(rc, "update metadata: pinPage");

    Bytes* sb = newBytes(page->data, PAGE_SIZE);

    // write metadata to page
    _writeSchema(sb, f->schema);

    // write attributes
    writeBytesInt(sb, f->totalNumTuples);

    // queue of avaliable blocks
    writeBytesInt(sb, f->headAvaliableBlock);
    writeBytesInt(sb, f->tailAvaliableBlock);

    markDirty(f->pool, page);
    unpinPage(f->pool, page);
    free(page);
    free(sb);
    return RC_OK;
}

///////////////////////////////////////////////////////////////////////////////

RC createTableFile(char* fileName, Schema* s) {
    ASSERT_NULL(fileName, "createTableFile:name");
    ASSERT_NULL(s, "createTableFile:s");

    RC rc;
    rc = createPageFile(fileName);
    CHECK_RC(rc, "createTableFile: create");

    BM_BufferPool* pool = MAKE_POOL();
    rc = initBufferPool(pool, fileName, PAGE_BUFFER_SIZE, PAGE_BUFFER_STRATEGY, NULL);
    CHECK_RC(rc, "createBlockFile: init pool");

    rc = initMetadata(pool, s);
    CHECK_RC(rc, "createBlockFile: initMetadata");

    rc = shutdownBufferPool(pool);
    CHECK_RC(rc, "createTableFile: shutDownBufferPool");

    free(pool);
    return RC_OK;
}

TableFile* openTableFile(char* fileName) {
    ASSERT_NULL(fileName, "openTableFile:name");
    
    TableFile* f = malloc(sizeof(TableFile));
    f->pool = MAKE_POOL();

    RC rc = initBufferPool(f->pool, fileName, PAGE_BUFFER_SIZE, PAGE_BUFFER_STRATEGY, NULL);
    CHECK(rc);

    rc = loadMetadata(f);
    CHECK(rc);

    // _printMetadata(f);

    return f;
}

RC closeTableFile(TableFile* f) {
    ASSERT_NULL(f, "closeTableFile:f");
    
    RC rc = updateMetadata(f);
    CHECK_RC(rc, "closeTableFile: update metadata");

    rc = shutdownBufferPool(f->pool);
    CHECK_RC(rc, "closeTableFile: shutdown pool");

    free(f->pool);
    free(f->schema);
    free(f);
    return RC_OK;
}

RC deleteTableFile (char *fileName) {
    ASSERT_NULL(fileName, "deleteTableFile:name");

    return destroyPageFile(fileName);
}

///////////////////////////////////////////////////////////////////////////////

RC updateBlock(TableFile* f, Block* b) {
    ASSERT_NULL(f, "updateBlock:f");
    ASSERT_NULL(b, "updateBlock:b");

    seekBytes(b->buf, 0);

    // write header to block
    writeBytesInt(b->buf, b->numTuples);    
    writeBytesInt(b->buf, b->nextAvaliableBlock);
    writeBytes(b->buf, b->slotMarks, b->slotSize);
    // NOTICE: we don't need to write block content
    //      since new content has already be written
    //      in the block buffer by other functions.

    // mark dirty
    RC rc = markDirty(f->pool, b->page);
    CHECK_RC(rc, "updateBlock: markDirty");
    return RC_OK;
}

Block* pinBlock(TableFile* f, int pageNum) {
    ASSERT_NULL(f, "pinBlock:f");
    RC rc;
    Block* b = malloc(sizeof(Block));

    // page
    b->page = MAKE_PAGE_HANDLE();
    b->pageNum = pageNum;
    rc = pinPage(f->pool, b->page, pageNum);
    CHECK(rc);

    // bytes buffer
    b->buf = newBytes(b->page->data, PAGE_SIZE);

    // attributes
    b->offset = f->recordOffset;
    b->capacity = f->recordCapacity;
    b->recordSize = f->recordSize;
    b->slotSize = f->recordCapacity / MARK_BITS;
    if (f->recordCapacity % MARK_BITS) {
        b->slotSize += 1;
    }
    b->slotMarks = malloc(b->slotSize * sizeof(unsigned char));

    // block header
    if (getTotalNumPages(f->pool) <= pageNum) {
        // pin a new block
        // init header
        b->numTuples = 0;
        b->nextAvaliableBlock = -1; // TODO add this one to the end of queue
        memset(b->slotMarks, 0, b->slotSize);
        // write block
        rc = updateBlock(f, b);
        CHECK(rc);
    } else {
        // read block header from block
        seekBytes(b->buf, 0);
        readBytesInt(b->buf, &b->numTuples);
        readBytesInt(b->buf, &b->nextAvaliableBlock);
        readBytes(b->buf, b->slotMarks, b->slotSize);
    }

    // _printBlock(b);

    return b;
}

RC unpinBlock(TableFile *f, Block* block) {
    ASSERT_NULL(f, "unpinBlock:f");
    ASSERT_NULL(block, "unpinBlock:b");

    RC rc = updateBlock(f, block);
    CHECK_RC(rc, "unpinBlock: updateBlock");

    rc = unpinPage(f->pool, block->page);
    CHECK_RC(rc, "unpinBlock: unpinPage");

    free(block->slotMarks);
    free(block->page);
    free(block->buf);
    free(block);
    return rc;
}

///////////////////////////////////////////////////////////////////////////////

int _getSlotMark(Block* block, int i) {
    int j = i / MARK_BITS;
    int k = i % MARK_BITS;
    return (block->slotMarks[j] >> k) & 1;
}

void _setSlotMark(Block* block, int i, int mark) {
    int j = i / MARK_BITS;
    int k = i % MARK_BITS;
    block->slotMarks[j] |= 1 << k;
}

int _requestSlot(TableFile* f, Block* block) {
    ASSERT_NULL(f, "requestSlot:f");
    ASSERT_NULL(block, "requestSlot:b");

    if (block->capacity <= block->numTuples) {
        return -1;
    }

    int i;
    for (i = 0; i < block->capacity; i++) {
        int mark = _getSlotMark(block, i);
        if (mark == 0) {
            // found a empty slot
            _setSlotMark(block, i, 1);
            
            RC rc = updateBlock(f, block);
            CHECK_RC(rc, "requestSlot: updateBlock");

            return i;
        }
    }
    return -1;  // not found, never reach here
}


Block* _getAvaliableBlock(TableFile* f) {
    ASSERT_NULL(f, "getAvaliableBlock:f");

    // queue is empty
    // if (f->headAvaliableBlock > 0) {
    //     // create a new block at the end of file
    //     block = pinBlock(f, getTotalNumPages(f->pool));
    //     // TODO add to the tail of queue
    // }

    // // pin block
    // block = pinBlock(f, f->headAvaliableBlock);
    // return block;
 
    //////////////////////////////////////////////
    Block* block;
    for (int i = 1; i <= getTotalNumPages(f->pool); i++) {
        block = pinBlock(f, i);

        if (block->numTuples < block->capacity) {
            return block;
        }
        unpinBlock(f, block);
    }
    return NULL;    // never reach here
}

RID requestSlot(TableFile* f) {
    ASSERT_NULL(f, "requestSlot:f");
    
    Block* block = _getAvaliableBlock(f);

    RID id;
    if (block->capacity <= block->numTuples) {
        id.page = -1;
        id.slot = -1;
        
        unpinBlock(f, block);
        return id;  
    }

    int i;
    for (i = 0; i < block->capacity; i++) {
        int mark = _getSlotMark(block, i);
        if (mark == 0) {
            // found a empty slot
            _setSlotMark(block, i, 1);
            RC rc = updateBlock(f, block);
            CHECK(rc);

            id.page = block->pageNum;
            id.slot = i;
            
            unpinBlock(f, block);
            return id;
        }
    }

    unpinBlock(f, block);
    id.page = -1;
    id.slot = -1;
    return id;
}

RC releaseSlot(TableFile* f, RID id) {
    ASSERT_NULL(f, "releaseSlot:f");
    
    Block* block = pinBlock(f, id.page);
    
    // set mark to empty
    _setSlotMark(block, id.slot, 0);
    
    // update
    RC rc = updateBlock(f, block);
    CHECK_RC(rc, "releaseSlot: updateBlock");

    unpinBlock(f, block);

    return RC_OK;
}

char* _locatSlot(Block* b, int slot) {
    return b->page->data + b->offset + slot * b->recordSize;
}

///////////////////////////////////////////////////////////////////////////////

int tableFileNumTuples(TableFile* f) {
    return f->totalNumTuples;
}

RC tableFileInsertRecord(TableFile* f, Record *record) {
    // find a free slot for record
    RID id = requestSlot(f);
    record->id = id;

    Block* block = pinBlock(f, id.page);

    RC rc = tableFileUpdateRecord(f, record);
    CHECK_RC(rc, "tableFileInsertRecord: update");

    // printf("InsertRecord: [%d,%d] slotSize %d, cap %d\n", id.page, id.slot, block->slotSize, block->capacity);

    // add count
    block->numTuples += 1;
    f->totalNumTuples += 1;

    rc = unpinBlock(f, block);
    CHECK_RC(rc, "tableFileInsertRecord: unpin");
    return RC_OK;
}

RC tableFileDeleteRecord(TableFile* f, RID id) {
    // release slot
    RC rc = releaseSlot(f, id);
    CHECK_RC(rc, "tableFileDeleteRecord");

    Block* block = pinBlock(f, id.page);

    block->numTuples -= 1;
    f->totalNumTuples -= 1;

    unpinBlock(f, block);
    CHECK_RC(rc, "tableFileDeleteRecord: unpin");
    return RC_OK;
}

RC tableFileUpdateRecord(TableFile* f, Record *r) {
    Block* block = pinBlock(f, r->id.page);

    // write record to slot
    char* data = _locatSlot(block, r->id.slot);
    memcpy(data, r->data, f->recordSize);

    RC rc = updateBlock(f, block);
    CHECK_RC(rc, "update record: updateBlock");

    rc = unpinBlock(f, block);
    CHECK_RC(rc, "update record: unpin");
    return RC_OK;
}

RC tableFileGetRecord(TableFile* f, RID id, Record *r) {
    // pin
    Block* block = pinBlock(f, id.page);

    // copy data from block
    char* data = _locatSlot(block, id.slot);
    memcpy(r->data, data, f->recordSize);

    // printf("getRecord: [%d,%d] pos %d, offset %d, cap %d, rsize %d\n", 
        // id.page, id.slot, (data - block->page->data), block->offset, block->capacity, block->recordSize);

    // unpin
    unpinBlock(f, block);
    return RC_OK;
}
