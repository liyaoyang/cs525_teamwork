#include "bytes_buffer.h"

#include <stdlib.h>

Bytes* newBytes(char* buff, int len) {
    Bytes* s = malloc(sizeof(Bytes));
    s->buff = buff;
    s->len = len;
    s->idx = 0;
    return s;
}

int seekBytes(Bytes* s, int offset) {
    if (offset < 0 || s->len <= offset) {
        return -1;
    }
    s->idx = offset;
    return offset;
}

int writeBytes(Bytes* s, char *data, int len) {
    int i;
    for (i = 0; i < len && s->idx < s->len; i++, s->idx++) {
        s->buff[s->idx] = data[i];
    }
    return i; 
}

int writeBytesInt(Bytes* s, int v) {
    int i, size = sizeof(int);
    char data[size + 1];
    for (i = 0; i < size; i++, v >>= 8) {
        data[i] = v & 0xFF;
    }
    int ret = writeBytes(s, data, size);
    if (ret != size) {
        return -1;
    }
    return 0;
}

int writeBytesBool(Bytes* s, int b) {
    char data[2];
    if (b) {
        data[0] = 0xFF;
    } else {
        data[0] = 0;
    }
    int ret = writeBytes(s, data, 1);
    if (ret != 1) {
        return -1;
    }
    return 0;
}

int readBytes(Bytes* s, char *data, int len) {
    int i;
    for (i = 0; i < len && s->idx < s->len; i++, s->idx++) {
        data[i] = s->buff[s->idx];
    }
    data[i] = '\0';
    return i;
}

int readBytesInt(Bytes* s, int* v) {
    int i, size = sizeof(int);
    char data[size + 1];
    int ret = readBytes(s, data, size);
    if (ret != size) {
        return -1;
    }
    *v = 0;
    int ch;
    for (i = 0; i < size; i++) {
        ch = 0xFF & data[i];
        *v |= ch << (i*8);
    }
    return 0;
}

int readBytesBool(Bytes* s, int* b) {
    char data[2];
    int ret = readBytes(s, data, 1);
    if (ret != 1) {
        return -1;
    }
    if (data[0]) {
        *b = 1;
    } else {
        *b = 0;
    }
    return 0;
}