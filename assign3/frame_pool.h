#ifndef FRAME_POOL_H
#define FRAME_POOL_H

#include <stdbool.h>
#include <pthread.h>

#include "buffer_mgr.h"
#include "dberror.h"
#include "map.h"

// Frame is a single page frame in memory
typedef struct BM_Frame {
    int pageNum;            // page number, -1 means not in use
    char data[PAGE_SIZE];   // frame data in memory

    int fix;                // pin count
    bool dirty;             // if been modified

    int stratMark;          // reserved for replacement strategy

    pthread_mutex_t lock;   // lock for a frame
} BM_Frame;

// Frame Pool manages data structure of frames in memory
typedef struct BM_FramePool {
    int maxFrames;      // total number of frames in pool
    int usedFrames;     // current cached frames
    BM_Frame* frames;   // array of page frames
    Map* map;           // map[pageNum] -> index of pool
    ReplacementStrategy strategy;
    pthread_mutex_t lock;   // lock for framePool
} BM_FramePool;

///////////////////////////////////////////////////////////////////////////////

// create frame pool.
BM_FramePool* newFramePool(int maxFrames, ReplacementStrategy strategy);

// release frame pool.
void freeFramePool(BM_FramePool* pool);

// request a frame by replacement strategy.
BM_Frame* framePoolRequest(BM_FramePool* pool);

// get a frame by page number. return NULL if pageNum not in pool.
BM_Frame* framePoolGet(BM_FramePool* pool, int pageNum);

// set a frame with a pageNum.
void framePoolSet(BM_FramePool* pool, BM_Frame* f, int pageNum);


#endif
