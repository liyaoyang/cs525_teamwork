#include <stdio.h>
#include <stdlib.h>

#include "dberror.h"
#include "bytes_buffer.h"

void testBytesBuffer() {
	char* data = malloc(PAGE_SIZE);
	Bytes* b = newBytes(data, PAGE_SIZE);

	// write and read int
	seekBytes(b, 0);
	for (int i = -100; i < 100; i++) {
		writeBytesInt(b, i);
	}
	seekBytes(b, 0);
	for (int i = -100; i < 100; i++) {
		int v;
		readBytesInt(b, &v);
		if (v != i) {
			printf("testBytesBuffer: int, %d expected %d\n", i, v);
		}
	}

	// unsigned char
	int markSize = 256;
	unsigned char* marks = malloc(markSize);
	for (int i = 0; i < markSize; i++) {
		marks[i] = i;
	}
	seekBytes(b, 0);
	writeBytes(b, marks, markSize);

	seekBytes(b, 0);
	readBytes(b, marks, markSize);
	for (int i = 0; i < markSize; i++) {
		if (marks[i] != i) {
			printf("testBytesBuffer: unsigned char, %d expected %d\n", marks[i], i);
		}
	}

	printf("TEST bytes buffer DONE.\n");

	free(data);
	free(b);
}

///////////////////////////////////////////////////////////////////////////////

int main() {
	
	testBytesBuffer();

    return 0;
}