#ifndef REPLACEMENT_STRATEGY_H
#define REPLACEMENT_STRATEGY_H

#include "frame_pool.h"
#include "buffer_mgr.h"

RC initStrategy(ReplacementStrategy strategy, BM_FramePool* pool, void* param);

// When an event happens, this function will be called to notify strategy.
// Stragety handle events in different ways depend on what type of strategy it is.
void onStrategyEventPin(ReplacementStrategy strategy, BM_Frame* frame);
void onStrategyEventUnpin(ReplacementStrategy strategy, BM_Frame* frame);

// evict a frame
// return the frame of the evicted frame. return NULL if can not evict any one.
BM_Frame* evict(ReplacementStrategy strategy, BM_FramePool* pool);


#endif