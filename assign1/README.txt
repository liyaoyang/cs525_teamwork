
For this assignment, all our works are written in one file 'storage_mgr.c'.
This document describes some crucial ideas of our work.

Task Assignment:
    Jing Chen   A20381546:  File operation functions.
    Yuan Ma     A20394062:  Read file operation functions.
    Qi Gao      A20409777:  Write file operation functions.
    Liyao Yang  A20411097:  Bug fix, code reorganizing, Makefile and README.txt.

Makefile:
    Use the following commands
    make        Build sorece files and generate a binary file 'test_assign1'
    make test   Build and run test file.
    make clean  Clean object files.

SM_Filehandle->mgmtInfo:
    This field is only used to store file pointer FILE*. Check this field to know if it is a valid handle.

createPageFile:
    Create a file with one empty block. If file already exists, truncate all the content.

openPageFile, closePageFile:
    Open a file and store the pointer FILE* into mgmtInfo of FileHandle.
    Close a file and set the field mgmtInfo to NULL.

readBlock, writeBlock:
    Other read and write functions use these two functions underneath.