#include<stdio.h>
#include<stdlib.h>
#include"storage_mgr.h"
#include"dberror.h"
#include <unistd.h>


///////////////////////////////////////////////////////////////////////////////
// local helper

#define BYTE_SIZE 1

#define CHECK_NULL_MSG(ptr, rc, msg)							\
		do {									\
			if (ptr == NULL) {					\
				THROW(rc, msg);					\
			}									\
		} while(0);

#define CHECK_NULL(ptr)     \
        CHECK_NULL_MSG(ptr, -1, "NULL pointer")

#define CHECK_FILE_HANDLE(h)         \
        do {                    \
            CHECK_NULL_MSG(h, RC_FILE_HANDLE_NOT_INIT, "handle==NULL"); \
            CHECK_NULL_MSG(h->mgmtInfo, RC_FILE_HANDLE_NOT_INIT, "mgmtInfo==NULL"); \
        } while(0);


RC _writeEmptyBlock(FILE* pf){
    CHECK_NULL(pf);
    
    char* mem = (char*)calloc(PAGE_SIZE, BYTE_SIZE);

    // write empty data
    size_t r = fwrite(mem, BYTE_SIZE, PAGE_SIZE, pf);
    
    free(mem);
    
    if (r != PAGE_SIZE) {
        THROW(RC_WRITE_FAILED, "writeEmptyBlock: write failed");
    }
    return RC_OK;
}

int _seekToPage(FILE* pf, int pageNum) {
    long pos = pageNum * BYTE_SIZE * PAGE_SIZE;
    return fseek(pf, pos, SEEK_SET); 
}

FILE* _getFile(SM_FileHandle* fHandle) {
    return (FILE*)(fHandle->mgmtInfo);
}

///////////////////////////////////////////////////////////////////////////////
// implementation of storage_mgr.h

void initStorageManager() {
	// DO NOTHING
}

RC createPageFile(char *fileName) {
    CHECK_NULL(fileName);

    // create a new file. 
    // clear file content if file exists.
    FILE* pf = fopen(fileName,"w");
    if (pf == NULL) {
        THROW(-1, "createPageFile: file not create");
    }

    // write one page block
    _writeEmptyBlock(pf);

    // close file
    fclose(pf); 
    return RC_OK;
}

RC openPageFile(char *fileName, SM_FileHandle *fHandle) {
    CHECK_NULL(fileName);    
    CHECK_NULL(fHandle);    // only check fHandle

    // open file for read and write
    FILE * pf = fopen(fileName, "r+");
    if ( pf == NULL) {
        // find not found
        return RC_FILE_NOT_FOUND;
    }

    // get file length
    fseek(pf, 0L, SEEK_END);
    int filelength = ftell(pf);

    // move to the beginning of file
    _seekToPage(pf, 0);

    fHandle->totalNumPages = filelength / PAGE_SIZE;
    fHandle->curPagePos = 0;
    fHandle->fileName = fileName;
    fHandle->mgmtInfo = pf;
    return RC_OK;
}

RC closePageFile(SM_FileHandle *fHandle) {
    CHECK_FILE_HANDLE(fHandle);
    
    fclose((FILE*)(fHandle->mgmtInfo));
    fHandle->mgmtInfo = NULL;
    return RC_OK;  
}

RC destroyPageFile(char *fileName) {
    CHECK_NULL(fileName);

    if (remove(fileName) != 0) {
        THROW(-1, "destroyPageFile: pagefile not deleted");
    }
    return RC_OK;
}

RC readBlock(int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    CHECK_NULL(memPage);

    if (fHandle->totalNumPages <= pageNum || pageNum < 0) {
        return RC_READ_NON_EXISTING_PAGE;
    }

    FILE* pf = _getFile(fHandle);

    // locate to the block
    if (0 != _seekToPage(pf, pageNum)) {
        THROW(RC_READ_NON_EXISTING_PAGE, "readBlock: seek failed");
    }

    // read one page from file
    size_t r = fread(memPage, BYTE_SIZE, PAGE_SIZE, pf);
    if (r != PAGE_SIZE) {
        THROW(-1, "readBlock: read page failed");
    }
    return RC_OK;
}

int getBlockPos(SM_FileHandle *fHandle){
    CHECK_FILE_HANDLE(fHandle);
    return fHandle->curPagePos;
}

RC readFirstBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    CHECK_NULL(memPage);
    return readBlock(0, fHandle, memPage);
}

RC readPreviousBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    CHECK_NULL(memPage);
    if(fHandle->curPagePos = 0){
        return RC_READ_NON_EXISTING_PAGE;
    }
    fHandle->curPagePos--;  // move curPagePos
    return readBlock(fHandle->curPagePos, fHandle, memPage);
}

RC readCurrentBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    CHECK_NULL(memPage);
    return readBlock(fHandle->curPagePos, fHandle, memPage);
}

RC readNextBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    CHECK_NULL(memPage);
    if (fHandle->curPagePos >= fHandle->totalNumPages - 1) {
        return RC_READ_NON_EXISTING_PAGE;
    }
    fHandle->curPagePos++;  // move curPagePos
    return readBlock(fHandle->curPagePos, fHandle, memPage);
}

RC readLastBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    CHECK_NULL(memPage);
    return readBlock(fHandle->totalNumPages - 1, fHandle, memPage);
}

RC writeBlock(int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    CHECK_NULL(memPage);

    if (pageNum < 0 || fHandle->totalNumPages <= pageNum) {
        THROW(RC_WRITE_FAILED, "writeBlock: invalid pageNum");
    }

    FILE* pf = _getFile(fHandle);

    // locate to the block
    if (0 != _seekToPage(pf, pageNum)) {
        THROW(RC_WRITE_FAILED, "writeBlock: seek failed");
    }
    
    // write to file
    size_t r = fwrite(memPage, BYTE_SIZE, PAGE_SIZE, pf);
    if (r != PAGE_SIZE) {
        THROW(RC_WRITE_FAILED, "writeBlock: write file failed");
    }
    return RC_OK;
}

RC writeCurrentBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    CHECK_NULL(memPage);
    return writeBlock(fHandle->curPagePos, fHandle, memPage);
}

RC appendEmptyBlock(SM_FileHandle *fHandle){
    CHECK_FILE_HANDLE(fHandle);

    FILE* pf = _getFile(fHandle);

    // locate to the end of file
    if (0 != _seekToPage(pf, fHandle->totalNumPages)) {
        THROW(-1, "appendEmptyBlock: seek failed");
    }

    // write an empty block to the end of file
    RC rc = _writeEmptyBlock(pf);
    if (rc != RC_OK) {
        return rc;
    }
    fHandle->totalNumPages++;
    return RC_OK;
}

RC ensureCapacity(int numberOfPages, SM_FileHandle *fHandle) {
    CHECK_FILE_HANDLE(fHandle);

    // already have enough capacity
    if (fHandle->totalNumPages >= numberOfPages) {
        return RC_OK;
    }

    // append empty blocks one by one
    for (int i = 0; i < numberOfPages - fHandle->totalNumPages; i++) {
        RC rc = appendEmptyBlock(fHandle);
        if (rc != RC_OK)
            return rc;
    }
    return RC_OK;
}