#include "replacement_strategy.h"

#include <limits.h>
#include <pthread.h>
#include "buffer_mgr.h"
#include "util.h"

// global counter 
int g_stratCounter;

// clock hand for CLOCK strategy
int g_stratClockHand;

// lock for strategy module
pthread_mutex_t g_stratLock;

RC initStrategy(ReplacementStrategy strategy, BM_FramePool* pool, void* param)
{
    ASSERT_NULL(pool, "initStrategy:pool");

    switch (strategy) {
        case RS_FIFO:
        case RS_LRU:
        case RS_LFU:
        case RS_CLOCK:
            break;
        default:
            THROW(-1, "initStrategy: strategy is not supported");
    }
    
    g_stratCounter = 0;
    
    g_stratClockHand = 0;

    if (pthread_mutex_init(&g_stratLock, NULL) != 0) {
        THROW(-1, "initStrategy: mutex init has failed");
    }

    return RC_OK;
}

void onStrategyEventPin(ReplacementStrategy strategy, BM_Frame* frame)
{
    ASSERT_NULL(frame, "onStrategyEventPin:frame");

    // NOTICE: We don't need to lock frame->lock here, since the caller has already held the lock.

    bool isNew = frame->fix == 1;

    switch (strategy) {
        case RS_FIFO:
            if (isNew) {
                pthread_mutex_lock(&g_stratLock);
                frame->stratMark = ++g_stratCounter;
                pthread_mutex_unlock(&g_stratLock);
            }
            break;

        case RS_LRU:
            // DO NOTHING
            break;

        case RS_CLOCK:
            frame->stratMark = 1;
            break;

        case RS_LFU:
            frame->stratMark += 1;
            break;

        case RS_LRU_K:
            break;

        default:
            break;
    }
}

void onStrategyEventUnpin(ReplacementStrategy strategy, BM_Frame* frame)
{
    ASSERT_NULL(frame, "onStrategyEventUnpin:frame");

    // NOTICE: We don't need to lock frame->lock here, since the caller has already held the lock.

    bool isIdle = frame->fix == 0;

    switch (strategy) {
        case RS_FIFO:
            // DO NOTHING
            break;

        case RS_LRU:
            if (isIdle) {
                pthread_mutex_lock(&g_stratLock);
                frame->stratMark = ++g_stratCounter;
                pthread_mutex_unlock(&g_stratLock);
            }
            break;

        case RS_CLOCK:
            // DO NOTHING
            break;

        case RS_LFU:
            break;

        case RS_LRU_K:
            break;

        default:
            break;
    }
}

BM_Frame* evict(ReplacementStrategy strategy, BM_FramePool* pool)
{
    ASSERT_NULL(pool, "replacementStrategy.evict");

    // NOTICE: We don't need to lock pool->lock here, since the caller has already held the lock.

    BM_Frame* frame = NULL;
    int minMark = INT_MAX;

    switch (strategy) {
        case RS_FIFO:
        case RS_LRU:
        case RS_LFU:
            // Find the frame with minimal mark, whose pin count is zero.
            // For FIFO, it is the earliest pined frame.
            // For LRU, it is the earlist idled frame.
            // For LFU, it is the least frequently used frame.
            for (int i = 0; i < pool->maxFrames; i++) {
                BM_Frame* f = &pool->frames[i];
                if (minMark > f->stratMark && f->fix == 0) {
                    minMark = f->stratMark;
                    frame = f;
                }
            }
            break;

        case RS_CLOCK:
            pthread_mutex_lock(&g_stratLock);
            for (int i = 0; i < pool->maxFrames * 2; i++) {
                BM_Frame* f = &pool->frames[g_stratClockHand];
                // move clock hand one position advance
                g_stratClockHand = (g_stratClockHand + 1) % pool->maxFrames;

                if (f->fix != 0) // skip frame whose fix is not 0
                    continue;

                if (f->stratMark == 0) { // reference is 0
                    frame = f;
                    break;
                }
                f->stratMark = 0;   // reset reference to 0
            }
            pthread_mutex_unlock(&g_stratLock);
            break;

        case RS_LRU_K:
            break;

        default:
            break;
    }

    // reset strategy mark when evicted
    if (frame != NULL)
        frame->stratMark = 0;   

    return frame;
}
