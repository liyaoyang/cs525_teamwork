#ifndef MAP_H
#define MAP_H

#define MAP_NOT_FOUND -1
#define MAP_MAX_SIZE 1024*1024*10   // 10M

typedef struct Map {
    int m[MAP_MAX_SIZE];
} Map;

// create a map with initial size
Map* newMap(int size);

// free up space
void freeMap(Map* map);

// get a value associated with key from map. return MAP_NOT_FOUND if not exist.
int mapGet(Map* map, int key);

// put a key value pair into map. return old value.
int mapPut(Map* map, int key, int value);

#endif
