
We implemented extra replacement strategies CLOCK and LFU, and we made it thread safety.
Also we added some test cases for these functions.

Task Assignment:
    Jing Chen   A20381546:  Replacement strategy.
    Yuan Ma     A20394062:  Map and statistic functions.
    Qi Gao      A20409777:  Frame pool.
    Liyao Yang  A20411097:  Buffer manager, thread safety.

Makefile:
    Use the following commands
    make        Build sorece files and generate a binary file 'test_assign2'
    make run    Build and run test file.
    make clean  Clean object files.

Data structure:
    BM_BufferPool
        |- MgmtData 
            |- SM_FileHandle
            |- BM_FramePool
                |- Map
                |- ReplacementStrategy
                |- BM_Frame[]
                    |- int pageNum
                    |- data[PAGE_SIZE]
                    |- int fix
                    |- bool dirty

Code organization:
    buffer_mgr.c
        Implementation of buffer manager interfaces. 
    frame_pool.c:
        FramePool manages a pool of Frames. It provides basic operations of the pool, 
        also use replacement strategy to evict frame.
    map.c:
        An easy implementation of map, which is used to map maintain a mapping
        between page numbers and page frames.
    replacement_strategy.c
        Implementation of several replacement strategy algorithms.





