
#include "map.h"

#include <stdlib.h>

Map* newMap(int size)
{
    Map* map = (Map*)malloc(sizeof(Map));
    for (int i = 0; i < MAP_MAX_SIZE; i++) {
        map->m[i] = MAP_NOT_FOUND;
    }
    return map;
}

void freeMap(Map* map)
{
    free(map);
}

// get a value associated with key from map. return MAP_NOT_FOUND if not exist.
int mapGet(Map* map, int key)
{
    if (key < 0 || MAP_MAX_SIZE <= key)
        return MAP_NOT_FOUND;
    return map->m[key];
}

// put a key value pair into map. return old value.
int mapPut(Map* map, int key, int value)
{
    if (key < 0 || MAP_MAX_SIZE <= key)
        return MAP_NOT_FOUND;
    int old = map->m[key];
    map->m[key] = value;
    return old;
}

