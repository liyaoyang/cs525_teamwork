
#include <stdlib.h>
#include <pthread.h>

#include "buffer_mgr.h"
#include "storage_mgr.h"
#include "frame_pool.h"
#include "util.h"
#include "replacement_strategy.h"

typedef struct MgmtData {
    SM_FileHandle*  fileHandle;
    BM_FramePool*   framePool;

    PageNumber* statFrameContent;
    bool*       statDirtyFlags;
    int*        statFixCounts;
    int         statNumReadIO;
    int         statNumWriteIO;

    pthread_mutex_t fileHandleLock;
} MgmtData;

///////////////////////////////////////////////////////////////////////////////
// local helper

MgmtData* _MgmtData(BM_BufferPool *const bm) {
    MgmtData* md = (MgmtData*) bm->mgmtData;
    ASSERT_NULL(md, "_MgmtData");
    return md;
}

SM_FileHandle* _fileHandle(BM_BufferPool *const bm) {
    ASSERT_NULL(bm, "_fileHandle:bm");
    ASSERT_NULL(bm->mgmtData, "_fileHandle:mgmtData");
    MgmtData* md = (MgmtData*) bm->mgmtData;
    return md->fileHandle;
}

BM_FramePool* _framePool(BM_BufferPool *const bm) {
    ASSERT_NULL(bm, "_framePool:bm");
    ASSERT_NULL(bm->mgmtData, "_framePool:mgmtData");
    MgmtData* md = (MgmtData*) bm->mgmtData;
    return md->framePool;
}

void _lockFileHandle(BM_BufferPool *const bm) {
    pthread_mutex_lock(&_MgmtData(bm)->fileHandleLock);   
}

void _unlockFileHandle(BM_BufferPool *const bm) {
    pthread_mutex_unlock(&_MgmtData(bm)->fileHandleLock);
}

void _lockFramePool(BM_FramePool* pool) {
    pthread_mutex_lock(&pool->lock);
}

void _unlockFramePool(BM_FramePool* pool) {
    pthread_mutex_unlock(&pool->lock);
}

void _lockFrame(BM_Frame* frame) {
    pthread_mutex_lock(&frame->lock);
}

void _unlockFrame(BM_Frame* frame) {
    pthread_mutex_unlock(&frame->lock);
}

///////////////////////////////////////////////////////////////////////////////
// Buffer Manager Interface Pool Handling

RC initBufferPool(BM_BufferPool *const bm, char * pageFileName, 
		const int numPages, ReplacementStrategy strategy,
		void *stratData) 
{   
    ASSERT_NULL(bm, "initBufferPool:bm");
    ASSERT_NULL(pageFileName, "initBufferPool:pageFileName");

    // init file handle
    SM_FileHandle* fh = calloc(1, sizeof(SM_FileHandle));

    RC code = openPageFile(pageFileName, fh);
    if (code != RC_OK) 
    {
        free(fh);
        THROW(code, "initBufferPool.openPageFile()");
    }

    // make ManagementData
    MgmtData* md = calloc(1, sizeof(MgmtData));
    md->fileHandle = fh;
    md->framePool = newFramePool(numPages, strategy);

    // init strategy
    code = initStrategy(strategy, md->framePool, stratData);
    if (code != RC_OK) 
    {
        free(fh);
        freeFramePool(md->framePool);
        return code;
    }

    // init statistic
    md->statFrameContent = calloc(numPages, sizeof(PageNumber));
    md->statDirtyFlags = calloc(numPages, sizeof(bool));
    md->statFixCounts = calloc(numPages, sizeof(int));
    md->statNumReadIO = 0;
    md->statNumWriteIO = 0;

    // init locks
    pthread_mutex_init(&md->fileHandleLock, NULL);

    // init buffer maneger
    bm->mgmtData = md;
    bm->pageFile = pageFileName;
    bm->numPages = numPages;
    bm->strategy = strategy;

    return RC_OK;
}

RC shutdownBufferPool(BM_BufferPool *const bm) 
{
    // check pointer
    ASSERT_NULL(bm, "shutdowBufferPool:bm");
    ASSERT_NULL(bm->mgmtData, "shutdowBufferPool:mgmtData");

    MgmtData* md = _MgmtData(bm);

    // flush all data
    forceFlushPool(bm);

    // free frame pool    
    freeFramePool(md->framePool);
    md->framePool = NULL;

    // free statistic
    free(md->statFrameContent);
    free(md->statDirtyFlags);
    free(md->statFixCounts);

    // destroy locks
    pthread_mutex_destroy(&md->fileHandleLock);

    // close page file
    RC code = closePageFile(md->fileHandle);
    if (code != RC_OK) 
        THROW(code, "shutdownBufferPool.closePageFile");
    md->fileHandle = NULL;

    bm->mgmtData = NULL;
    return RC_OK;
}

RC _forceFrame(BM_BufferPool *const bm, BM_Frame* frame) 
{
    ASSERT_NULL(bm, "_forceFrame:bm");
    ASSERT_NULL(frame, "_forceFrame:frame");

    // no need to write
    if (!frame->dirty) {
        return RC_OK;
    }

    SM_FileHandle* fHandle = _fileHandle(bm);

    // write frame to disk
    _lockFileHandle(bm);
    RC code = writeBlock(frame->pageNum, fHandle, frame->data);
    _unlockFileHandle(bm);

    if (code == RC_OK)
        frame->dirty = false;

    // statistic
    _MgmtData(bm)->statNumWriteIO++;

    return code;
}

RC forceFlushPool(BM_BufferPool *const bm) 
{
    ASSERT_NULL(bm, "forceFlushPool:bm");

    BM_FramePool* pool = _framePool(bm);
    RC code;

    _lockFramePool(pool);

    for (int i = 0; i < pool->maxFrames; i++) 
    {
        BM_Frame* frame = &pool->frames[i];
        
        _lockFrame(frame);

        // force flush frame
        RC c = _forceFrame(bm, frame);
        
        _unlockFrame(frame);

        if (c != RC_OK) 
            code = c;
    }

    _unlockFramePool(pool);

    if (code != RC_OK) 
        THROW(code, "forceFlushPool.writeBlock");

    return RC_OK;
}

///////////////////////////////////////////////////////////////////////////////
// Buffer Manager Interface Access Pages

RC pinPage(BM_BufferPool *const bm, BM_PageHandle *const page, 
		const PageNumber pageNum)
{
    ASSERT_NULL(bm, "pinPage:bm");
    ASSERT_NULL(page, "pinPage:page");

    RC code;
    BM_FramePool* pool = _framePool(bm);
    SM_FileHandle* fHandle = _fileHandle(bm);

    // total number of pages
    _lockFileHandle(bm);
    int totalNumPages = fHandle->totalNumPages;
    _unlockFileHandle(bm);

    if (pageNum < 0 || totalNumPages < pageNum) {
        THROW(RC_READ_NON_EXISTING_PAGE, "pinPage fHandle->totalNumPages < pageNum");
    }

    // get frame from pool
    BM_Frame* frame = framePoolGet(pool, pageNum);

    // frame is already in memory
    if (frame != NULL) 
    {
        frame->fix++;

        // notice replacement strategy
        onStrategyEventPin(bm->strategy, frame);

        page->data = frame->data;
        page->pageNum = pageNum;
        return RC_OK;
    }

    // frame is not in memory
    // request a free space by applying replacement strategy
    frame = framePoolRequest(pool);

    // buffer is full
    if (frame == NULL) 
        THROW(-1, "pinPage: Buffer is full."); // TODO define RC_BUFFER_FULL

    _lockFrame(frame);
    
    // Dirty pages can be evicted from the pool if they have a fix count 0,
    // but have to be written back to disk before the eviction
    
    if (frame->dirty) {
        // force flush frame
        code = _forceFrame(bm, frame);

        if (code != RC_OK) {
            _unlockFrame(frame);
            THROW(code, "pinPage._forceFrame");
        }
    }

    
    // add one more block
    _lockFileHandle(bm);
    if (fHandle->totalNumPages == pageNum) {
        code = appendEmptyBlock(fHandle);
        if (code != RC_OK) {
            _unlockFileHandle(bm);
            _unlockFrame(frame);
            THROW(code, "pinPage.appendEmptyBlock");
        }
    }

    // load from storage manager
    code = readBlock(pageNum, fHandle, frame->data);
    if (code != RC_OK) {
        _unlockFileHandle(bm);
        _unlockFrame(frame);
        THROW(code, "pinPage.readBlock");
    }
    _unlockFileHandle(bm);

    // statistic
    _MgmtData(bm)->statNumReadIO++;

    // set fix count
    frame->fix = 1;

    // update frame pool
    framePoolSet(pool, frame, pageNum);
    
    // notice replacement strategy
    onStrategyEventPin(bm->strategy, frame);

    page->pageNum = pageNum;
    page->data = frame->data;

    _unlockFrame(frame);
    return RC_OK;
}

RC unpinPage(BM_BufferPool *const bm, BM_PageHandle *const page) 
{
    ASSERT_NULL(bm, "unpinPage:bm");
    ASSERT_NULL(page, "unpinPage:page");

    BM_FramePool* pool = _framePool(bm);

    // get frame from pool
    BM_Frame* frame = framePoolGet(pool, page->pageNum);
    
    if (frame == NULL)
        THROW(RC_READ_NON_EXISTING_PAGE, "unpinPage: page not found");

    _lockFrame(frame);

    if (frame->fix <= 0) {
        _unlockFrame(frame);
        THROW(-1, "unpinPage: fix<=0");
    }

    // decrease fix count
    frame->fix--;

    // notice strategy the unpin event
    onStrategyEventUnpin(bm->strategy, frame);

    _unlockFrame(frame);

    return RC_OK;
}

RC markDirty(BM_BufferPool *const bm, BM_PageHandle *const page) 
{
    ASSERT_NULL(bm, "markDirty:bm");
    ASSERT_NULL(page, "markDirty:page");

    BM_FramePool* pool = _framePool(bm);
    BM_Frame* frame = framePoolGet(pool, page->pageNum);
    
    if (frame == NULL) {
        THROW(RC_READ_NON_EXISTING_PAGE, "makeDirty: page not found");
    }

    _lockFrame(frame);
    
    frame->dirty = true;

    _unlockFrame(frame);

    return RC_OK;
}

RC forcePage(BM_BufferPool *const bm, BM_PageHandle *const page) 
{
    ASSERT_NULL(bm, "forcePage:bm");
    ASSERT_NULL(page, "forcePage:page");

    BM_FramePool* pool = _framePool(bm);
    BM_Frame* frame = framePoolGet(pool, page->pageNum);

    if (frame == NULL) 
        THROW(RC_READ_NON_EXISTING_PAGE, "forcePage: page not found");

    _lockFrame(frame);

    // force flush frames    
    RC code = _forceFrame(bm, frame);

    _unlockFrame(frame);

    return code;
}

int getTotalNumPages(BM_BufferPool *const bm) 
{
    ASSERT_NULL(bm, "getTotalNumPages:bm");
    return _MgmtData(bm)->fileHandle->totalNumPages;
}

///////////////////////////////////////////////////////////////////////////////
// Statistics Interface

PageNumber *getFrameContents (BM_BufferPool *const bm)
{
    BM_FramePool* pool = _framePool(bm);
    _lockFramePool(pool);
    for (int i = 0; i < bm->numPages; i++) {
        PageNumber pageNum = pool->frames[i].pageNum;
        _MgmtData(bm)->statFrameContent[i] = pageNum;
    }
    _unlockFramePool(pool);
    return _MgmtData(bm)->statFrameContent;
}

bool* getDirtyFlags(BM_BufferPool *const bm)
{
    BM_FramePool* pool = _framePool(bm);
    _lockFramePool(pool);
    for (int i = 0; i < bm->numPages; i++) {
        bool dirty = pool->frames[i].dirty;
        _MgmtData(bm)->statDirtyFlags[i] = dirty;
    }
    _unlockFramePool(pool);
    return _MgmtData(bm)->statDirtyFlags;
}

int* getFixCounts(BM_BufferPool *const bm)
{
    BM_FramePool* pool = _framePool(bm);
    _lockFramePool(pool);
    for (int i = 0; i < bm->numPages; i++) {
        int fix = pool->frames[i].fix;
        _MgmtData(bm)->statFixCounts[i] = fix;
    }
    _unlockFramePool(pool);
    return _MgmtData(bm)->statFixCounts;
}

int getNumReadIO(BM_BufferPool *const bm)
{
    return _MgmtData(bm)->statNumReadIO;
}

int getNumWriteIO(BM_BufferPool *const bm)
{
    return _MgmtData(bm)->statNumWriteIO;
}