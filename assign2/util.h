#include <stdlib.h>
#include "dberror.h"

#define CHECK_NULL_MSG(ptr, rc, msg)			\
		do {									\
			if (ptr == NULL) {					\
				THROW(rc, msg);					\
			}									\
		} while(0);

#define CHECK_NULL(ptr)     \
        CHECK_NULL_MSG(ptr, -1, "NULL pointer")

#define ASSERT_NULL(ptr, msg)        \
        do {                    \
            if (ptr == NULL) {  \
                printf("NULL %s\n", msg); \
                exit(0);        \
            }                   \
        } while(0);

